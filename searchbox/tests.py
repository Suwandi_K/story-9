from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait

import os
import time
from .models import Books

class HomepageUnitTest(TestCase):
    def test_home_page_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_add_books_to_database(self):
        book_id = 'ajHIkjyokW'
        like_count = 5
        title = "TOEFL Test 2020"
        publisher = "Roy Simatupang"
        Books.objects.create(
            book_id = book_id,
            title = title,
            publisher = publisher,
            published_date = None,
            like_count = like_count,
            image_link = '',
            authors = None,
            average_rating = None
        )

        data = Books.objects.get(title=title)
        self.assertEqual(str(data), title + ' - ' + book_id)

class HomepageFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)   

        super(HomepageFunctionalTest, self).setUp()     

    def tearDown(self):
        self.browser.quit()
        super(HomepageFunctionalTest, self).tearDown()

    def test_title_and_search_box_exist_in_homepage(self):
        self.browser.get(self.live_server_url + '/')
        self.browser.implicitly_wait(10)

        self.assertIn("Perpustakaan 1D", self.browser.title)

        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertIn('SEARCH', page_text)

    def test_search_box_show_books_with_name_and_like_book(self):
        self.browser.get(self.live_server_url + '/')
        time.sleep(5)
        
        query = self.browser.find_element_by_id("keyword")
        query.send_keys("Harry Potter")
        time.sleep(20) 

        book = self.browser.find_element_by_class_name('book-result')
        data = book.get_attribute('innerHTML')

        self.assertIn('Harry', data)

        book_title = self.browser.find_element_by_class_name('book-title').get_attribute('innerHTML')
        like_button = self.browser.find_element_by_class_name('book-like-button')
        
        like_button.click()
        time.sleep(15)
        liked_book_data = Books.objects.get(title=book_title)
        self.assertEqual(liked_book_data.like_count, 1)  

        self.browser.execute_script("localStorage.clear();")

        self.browser.get(self.live_server_url + '/')
        time.sleep(5)

        query = self.browser.find_element_by_id("keyword")
        query.send_keys("Harry Potter")
        time.sleep(20) 

        book = self.browser.find_element_by_class_name('book-result')
        data = book.get_attribute('innerHTML')

        self.assertIn('Harry', data)

        book_title = self.browser.find_element_by_class_name('book-title').get_attribute('innerHTML')
        like_button = self.browser.find_element_by_class_name('book-like-button')
        
        like_button.click()
        time.sleep(15)
        liked_book_data = Books.objects.get(title=book_title)
        self.assertEqual(liked_book_data.like_count, 2) 

        like_button.click()
        time.sleep(15)
        books = Books.objects.get(title=book_title)
        self.assertEqual(books.like_count , 1)  

    def test_modal_showing_most_liked_book(self):
        books_added = []
        books_id = ['ajHIkjyokW','ghUjtwPOkq','oKLuytWEfv','PoiudjHGtw','RTYwfvGhjk','LpwkJudhty','okwABchsgi','BujkLikonW','sjHUytwrTr','KUijdhbxDD']
        books_title = ["Complete English Grammer for the Toefl Test","Harry Potter dan pangeran berdarah-campuran","Harry Potter dan piala api","JERRY","Jerry Falwell","The Sports Business in The Pacific Rim","Algae","Food","Composition Notebook","Flight"]
        books_publisher = ["Galangpress Group","Gramedia Pustaka Utama","Gramedia Pustaka Utama","Loka Media","Simon and Schuster","Springer","Cambridge University Press","Polity","Createspace Independent Publishing Platform","Penguin"]
        like_count = [8,3,7,9,4,5,6,3,1,2]
        for i in range(10):
            book_id = books_id[i]
            title = books_title[i]
            publisher = books_publisher[i]
            Books.objects.create(
                book_id = book_id,
                title = title,
                publisher = publisher,
                published_date = None,
                like_count = like_count[i],
                image_link = '',
                authors = None,
                average_rating = None
            )

            books_added.append((title, like_count[i]))

        books_added = sorted(books_added, key = lambda books : books[1], reverse = True)
        top_5_book = books_added[0:5]
        self.browser.get(self.live_server_url + '/')
        time.sleep(20)
        
        modal_button = self.browser.find_element_by_id('modalButton')
        modal_button.click()
        time.sleep(25)

        modal_data = self.browser.find_element_by_id('topBookModal').get_attribute('innerHTML')
        for x in top_5_book:
            self.assertTrue(x[0] in modal_data)

        before_like_count = Books.objects.get(title=top_5_book[0][0]).like_count

        like_button_top_book = self.browser.find_element_by_xpath("//div[@id='topBookContainer']/div[1]/div/button")
        like_button_top_book.click()
        time.sleep(10)

        after_like_count = Books.objects.get(title=top_5_book[0][0]).like_count
        self.assertTrue(after_like_count == before_like_count+1)

    def test_unlike_book_bad_request(self):
        self.browser.get(self.live_server_url + '/')
        time.sleep(5)
        
        query = self.browser.find_element_by_id("keyword")
        query.send_keys("Harry Potter")
        time.sleep(20) 

        book = self.browser.find_element_by_class_name('book-result')
        data = book.get_attribute('innerHTML')

        self.assertIn('Harry', data)

        book_title = self.browser.find_element_by_class_name('book-title').get_attribute('innerHTML')
        like_button = self.browser.find_element_by_class_name('book-like-button')
        
        like_button.click()
        time.sleep(15)
        liked_book_data = Books.objects.get(title=book_title)
        self.assertEqual(liked_book_data.like_count, 1)  

        books = Books.objects.get(title=book_title).delete()

        like_button.click()
        time.sleep(15)
        
        book_like_button = self.browser.find_element_by_class_name('book-like-button').get_attribute('innerHTML')
        self.assertEqual(book_like_button , 'Error unlike book')