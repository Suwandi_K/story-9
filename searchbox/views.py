from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
from django.shortcuts import render
import json

from .models import Books

# Create your views here.

def index(request):
    return render(request, 'index.html')

@csrf_exempt
def api_like_book(request):
    data = json.loads(request.body)
    if 'etag' in data: # From searched book query
        if 'imageLinks' not in data['volumeInfo']:
            data['volumeInfo']['imageLinks'] = {
                'thumbnail' : ''
            }

        if 'publishedDate' not in data['volumeInfo']:
            data['volumeInfo']['publishedDate'] = None

        if 'authors' not in data['volumeInfo']:
            data['volumeInfo']['authors'] = [None]

        if 'publisher' not in data['volumeInfo']:
            data['volumeInfo']['publisher'] = None
        
        if 'averageRating' not in data['volumeInfo']:
            data['volumeInfo']['averageRating'] = None

        try:
            book = Books.objects.get(book_id=data['id'])
            book.like_count += 1
            book.save()
        except Books.DoesNotExist:
            Books.objects.create(
                book_id = data['id'],
                title = data['volumeInfo']['title'],
                publisher = data['volumeInfo']['publisher'],
                published_date = data['volumeInfo']['publishedDate'],
                like_count = 1,
                image_link = data['volumeInfo']['imageLinks']['thumbnail'],
                authors = data['volumeInfo']['authors'][0],
                average_rating = data['volumeInfo']['averageRating']
            )
        return HttpResponse()
    else: # From top books
        book = Books.objects.get(book_id=data['id'])
        book.like_count += 1
        book.save()
        json_data = {
            'likeCount' : book.like_count
        }
        return JsonResponse(json_data)

@csrf_exempt
def api_unlike_book(request):
    data = json.loads(request.body)
    try:
        book = Books.objects.get(book_id=data['id'])
        book.like_count -= 1
        book.save()
        json_data = {
            'likeCount' : book.like_count
        }
        return JsonResponse(json_data)
    except:
        return HttpResponseBadRequest()

def top_book(request):
    top_book = Books.objects.order_by('-like_count')[:5]
    data = []
    for book in top_book:
        data.append({
            'bookId' : book.book_id,
            'title' : book.title,
            'publisher' : book.publisher,
            'likeCount' : book.like_count,
            'imageLinks' : book.image_link,
        })
    json_data = {
        'topBook' : data
    }
    return JsonResponse(json_data, safe=False)
