from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.index, name='homepage'),
    path('api/v1/likebook/', views.api_like_book, name="api_like_book"),
    path('api/v1/unlikebook/', views.api_unlike_book, name="api_unlike_book"),
    path('api/v1/topbook/', views.top_book, name="top_books"),
]
